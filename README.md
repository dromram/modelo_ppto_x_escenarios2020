## PRESUPUESTO POR ESCENARIOS - ANÁLISIS

## OBEJTIVO:

Establecer perspectivas de crecimiento de ingresos basado en la implementación de algunas metodologías analíticas para la prima emitida de la solución de autos.

Estas perspectivas de crecimiento, estarán determinadas por la capacidad que tendrán las metodologías analíticas de explicar los sucesos e identificar las variables que inciden en los patrones de crecimiento y volatilidad de la prima, por medio de bandas se dará una estimación de crecimiento del presupuesto.

## PERSPECTIVAS

## Perspectiva 1: Crecimiento basado en la visión de los expertos:

El experto de la Gerencia de Autos en conjunto con Gestión Financiera analizan los siguientes factores para realizar el presupuesto.

- Incremento de tarifa para rentabilizar la cartera.
- Comportamiento de la prima en mercado de las aseguradoras.
- La entrada de nuevos competidores.
- La proyección de cartera para crédito de vehículos. (Bancolombia)
- Nuevas alianzas empresariales para pólizas colectivas. (Empresariales)
- Tasa de retención de clientes. (Individuales)
- Proyección de la inflación del país.

## ¿Porqué es importante esta perspectiva?

- Conocimiento profundo de la solución.
- Conocimiento del comportamiento del mercado de seguros de autos voluntarios.

## Oportunidades

- No permite establecer un horizonte de pronóstico.
- No es modelable a cambios macroeconómicos.
- No establece posibles escenarios de crecimiento.
- Carece de la posibilidad de establecer un intervalo de crecimiento.

## Perspectiva 2: Crecimiento basado en el estudio y entendimiento de patrones de comportamientos pasados.

Estudia el patrón de comportamiento que ha presentado la prima emitida en el pasado, aprendiendo de los ciclos estacionales (picos mensuales) y la tendencia de crecimiento desde el año 2014 a 2018. Finalmente, establece un horizonte de pronóstico con el objetivo de prever el comportamiento futuro de la prima.

Se modelan diversas series de tiempo a un nivel de confianza del 95%. La metodología selecciona la serie de tiempo con menor error de pronóstico a partir de aplicar el modelo seleccionado a una muestra de prueba tomada de la información inicial. (Las series se construyen sin información del primer semestre de 2019 y esta información real se usa para probar la capacidad de pronóstico de las series).

## ¿Porqué es importante esta perspectiva?

Aprende de los ciclos estacionales (picos mensuales) y la incidencia de estos en la prima anual.
Tiene en cuenta el patrón de crecimiento.
Genera una perspectiva de comportamiento a largo plazo (horizonte de hasta 5 años).
Permite establecer un intervalo de crecimiento.

## Oportunidades

- Es una metodología estática.
- No permite modelar el pronóstico bajo cambios macroeconómicos.
- No establece escenarios de crecimiento.

## Perspectiva 3: Crecimiento basado en el establecimiento de relaciones con el entorno macroeconómico

Encontrar como influyen diferentes variables macroeconómicas en la variación de la prima emitida.
Datos 2014 a 2019.
Se tomaron las variaciones de cada una de las variables macros.
Luego de ejecutar el modelo, se identifican las variables que más se relacionan con la variación de la Prima Emitida, se presupuestan los crecimientos de las variables macroeconómicas para el año siguiente y se estima la variación porcentual que tendría la Prima Emitida bajo los escenarios.

## ¿Porqué es importante esta perspectiva?

- Es una metodología dinámica.
- Permite modelar escenarios de crecimiento bajo cambios macroeconómicos.

## Oportunidades

Modelo muy sensible a las variables independientes
La cantidad de datos disponibles limita la complejidad que el modelo puede
Variables macroeconómicas - Crecimiento índice de la Industria automotriz, IPC, Crecimiento parque automotor, Petróleo, Tasa empleo, Tasa Desempleo, Tasa de interés, TRM, Ventas de Autos Nuevos, crecimientos meses inmediatamente anterior (1,2,3,4)


# Propuesta general del modelo

La información con la que se va a trabajar tendrá  3 "mundos" con variables diferentes las cuales podrían explicar el crecimiento de la prima emitida mes a mes y anual.

## Mundo 1: Variables del negocio 

Se consideran variables propias del negocio del cuales se tienen: Prima Emitida Colectivo , Prima Emitida Individual, Siniestralidad Colectivo, Siniestralidad Individual, Prima Emitida por canales, Bancaseguros y Empresarial adicionalmente se tiene la tarifa promedio de la póliza de Autos.

## Mundo 2: Variables del Mercado Asegurador

Se consideran variables del conocimiento del mercado asegurador, es decir como se comporta Sura con respecto al total del mercado asegurador frente a producción de primas en Autos y la interacción con el pareto de aseguradoras.

## Mundo 3: Variables Macroeconómicas

Se consideran variables del comportamiento del entorno macroeconómico del país, tales como: TRM, IPC, parque automotor, tasa de empleo y desempleo, tasa de interés, entre otras. 


Estos 3 mundos interactúan entre si para poder explicar las variaciones de la prima emitida durante un tiempo determinado , de acuerdo al resultado final se podrá estimar a 12 meses el presupuesto de crecimiento  mediante unas bandas de confianza  de la solución Autos. 
